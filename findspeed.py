# -*- coding: utf8 -*-
import os
import numpy as np


def getNum(the_string):
    return float(the_string[4:])


def loadSpeed(base):
    result = []
    file_list = os.listdir(base)
    length = len(file_list)
    for i, ff in enumerate(file_list):
        print i / float(length)
        with open(base + ff) as f:
            f.readline()
            c = getNum(f.readline())
            m = getNum(f.readline())
            data = np.loadtxt(base + ff)
            if len(data) > 1:
                result.append([m, c, np.polyfit(data[:, 0], data[:, 1], 1)[0]])
            else:
                result.append([m, c, 0])
    return np.asarray(result)

#if __name__ == "__main__":
    #the_data = loadSpeed("/Users/kevinyoung/Downloads/linespeed/")
