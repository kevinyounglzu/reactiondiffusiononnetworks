#ifndef PATCH_H
#define PATCH_H

#include "rand.h"
#include <iostream>

using namespace std;

class PatchBase
{
    protected:
        int patch_capa;
        int number;
        double initial_percent;
        double growth_rate;
        mylib::RandDouble randd;
        mylib::RandInt randint;
    public:
        PatchBase(int N, double f0, double r): patch_capa(N), number(N*f0), initial_percent(f0), growth_rate(r), randd(), randint(1, patch_capa) {}

        virtual double birth_rate() { return growth_rate; }
        virtual double death_rate() { return growth_rate; }
        virtual double getPercent() { return number / static_cast<double>(patch_capa); }
        virtual int getNumber() { return number; }

        virtual void reset() { number = static_cast<int>(patch_capa * initial_percent); }
        virtual void setF0(double f0) { initial_percent = f0; reset(); }
        virtual void setCapa(int N) { patch_capa=N; }
        virtual void setGrowthRate(double r) { growth_rate=r; }

        virtual int popOne()
        {
            if(number==0)
                return 0;

            --number;
            return 1;
        }
        virtual void pushOne() { ++number; }

        virtual int birthDeathProcess()
        {
            if(randint.gen() <= number)
            {
                double r = randd.gen();
                if(r < birth_rate())
                {
                    ++number;
                }
                else if(r < birth_rate() + death_rate())
                {
                    --number;
                }
            }
            return 1;
        }

        virtual void oneMCStep()
        {
            for(int i=0; i<patch_capa; ++i)
                birthDeathProcess();
        }

        virtual void sim(int steps, ostream& out)
        {
            reset();
            for(int i=0; i<steps; ++i)
            {
                out << getPercent() << endl;
                oneMCStep();
            }
        }
};

class AlleePatch: public PatchBase
{
    protected:
        double allee_threshold;
    public:
        AlleePatch(): PatchBase(1, 0, 0), allee_threshold(0) {}
        AlleePatch(int N, double f0, double r, double k):PatchBase(N, f0, r), allee_threshold(k) {}

        virtual void setAlleeK(double k) {allee_threshold=k;}

        virtual double birth_rate()
        {
            return growth_rate * getPercent() * (1+allee_threshold);
        }
        virtual double death_rate()
        {
            double percent = getPercent();
            return growth_rate * (percent * percent + allee_threshold) ;
        }
};

class AlleePatchSim
{
    public:
        int patch_capa;
        double patch_capacity;
        int number;
        double initial_percent;
        double growth_rate;
        double allee_threshold;
        mylib::RandDouble randd;
    public:
        AlleePatchSim(): patch_capa(1), patch_capacity(1.), number(0), initial_percent(0), growth_rate(0), allee_threshold(0), randd() {}
//        AlleePatchSim(int N, double f0, double r, double c): patch_capa(N), number(N*f0), initial_percent(f0), growth_rate(r), allee_threshold(c), randd() {}

        virtual double getPercent() { return number / patch_capacity; }
        virtual int getNumber() { return number; }

        virtual void reset() { number = static_cast<int>(patch_capacity * initial_percent); }
        virtual void setInitialPercent(double f0) { initial_percent = f0; reset(); }
        virtual void setCapacity(int N) { patch_capa=N; patch_capacity=static_cast<int>(N); }
        virtual void setGrowthRate(double r) { growth_rate=r; }
        virtual void setAlleeThreshold(double c) {allee_threshold=c;}

        virtual double birth_rate()
        {
            return growth_rate * getPercent() * (1+allee_threshold);
        }
        virtual double death_rate()
        {
            double percent = getPercent();
            return growth_rate * (percent * percent + allee_threshold) ;
        }

        virtual int popOne()
        {
            if(number==0)
                return 0;

            --number;
            return 1;
        }
        virtual void pushOne() { ++number; }

        virtual int birthDeathProcess()
        {
            if(randd.gen() < getPercent())
            {
                double r = randd.gen();
                if(r < death_rate())
                {
                    popOne();
                }
                else if(r < birth_rate() + death_rate())
                {
                    ++number;
                }
            }
            return 1;
        }

        virtual void oneMCStep()
        {
            for(int i=0; i<patch_capa; ++i)
                birthDeathProcess();
        }

        virtual void sim(int steps, ostream& out)
        {
            reset();
            for(int i=0; i<steps; ++i)
            {
                out << getPercent() << endl;
                oneMCStep();
            }
        }
};

class AlleePatchRK4
{
    public:
        double u; // the value
        double k1, k2, k3, k4; // rk4
        double h;

        AlleePatchRK4(): u(0.), k1(0.), k2(0.), k3(0.), k4(0.), h(0.) {}
        AlleePatchRK4(double u0, double interval): u(u0), k1(0.), k2(0), k3(0), k4(0), h(interval) {}

        double updateValue()
        {
            u = u + h * (k1 + 2*k2 + 2*k3 + k4) / 6.;
            return u;
        }
};


class AlleePatchRK4TwoKind
{
    public:
        double u, v; // the value
        double u1, u2, u3, u4; // rk4
        double v1, v2, v3, v4;
        double h;

        AlleePatchRK4TwoKind(): u(0.), v(0.), u1(0.), u2(0.), u3(0.), u4(0.), v1(0.), v2(0.), v3(0.), v4(0.), h(0.) {}
        AlleePatchRK4TwoKind(double u0, double v0, double interval): u(u0), v(v0), u1(0.), u2(0.), u3(0.), u4(0.), v1(0.), v2(0.), v3(0.), v4(0.), h(interval) {}

        double updateValue()
        {
            u = u + h * (u1 + 2*u2 + 2*u3 + u4) / 6.;
            v = v + h * (v1 + 2*v2 + 2*v3 + v4) / 6.;
            return u;
        }
};


class AlleePatchRK4VaryC
{
    public:
        double u; // the value
        double c; // the allee_threshold
        double k1, k2, k3, k4; // rk4
        double h;

        AlleePatchRK4VaryC(): u(0.), c(0), k1(0.), k2(0.), k3(0.), k4(0.), h(0.) {}
        AlleePatchRK4VaryC(double u0, double c0, double interval): u(u0), c(c0), k1(0.), k2(0), k3(0), k4(0), h(interval) {}

        double updateValue()
        {
            u = u + h * (k1 + 2*k2 + 2*k3 + k4) / 6.;
            return u;
        }
};


#endif
