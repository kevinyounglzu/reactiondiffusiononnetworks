#include <iostream>
#include <string>

#include "rand.h"
#include "parser.h"
#include "utils.h"
#include "mymath.h"

#include "Snap.h"

#include "snaphelper.h"
#include "patch.h"
//#include "wave.h"
#include "alleediffusion.h"

using namespace std;
using mylib::operator<<;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    int size = parser.getInt("size");
    int degree = parser.getInt("degree");
    PUNGraph graph = TSnap::GenRndDegK(size, degree);

    int agent_n = parser.getInt("agent_n");
    int steps = parser.getInt("steps");

    double r = parser.getDouble("r");
    double m = parser.getDouble("m");
    double c = parser.getDouble("c");
    double cutoutp = parser.getDouble("cutoutp");

    int repeat = parser.getInt("repeat");

    ofstream out;



    out.open(mylib::tapeFileName("./data/simregular/", ".txt", argv[1]));
    out << "# s=" << size << endl;
    out << "# r=" << r << endl;
    out << "# m=" << m << endl;
    out << "# c=" << c << endl;
    out << "# C=" << agent_n << endl;

    for(int i=0; i<repeat; ++i)
    {
        AlleeDiffusion ad(agent_n, r, c, m, cutoutp, graph, 50, 0.00001, 1.);
        ad.sim(steps);
        out << ad.getOcupition() << endl;
    }

    out.close();


    return 0;
}
