#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int grid_length = parser.getInt("l");

    double clow = parser.getDouble("clow");
    double chigh = parser.getDouble("chigh");
    double c_ratio = parser.getDouble("cratio");

    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);


    ofstream out;
    out.open(mylib::tapeFileName("./data/gridbic/", ".txt", argv[1]));

    out << "# l=" << clow << endl;
    out << "# h=" << chigh << endl;
    out << "# r=" << c_ratio << endl;
    out << "# m=" << mu << endl;
    out << "# s=" << grid_length << endl;


    // for snap
    PUNGraph graph = TSnap::GenGrid<PUNGraph>(grid_length, grid_length, false);
    AlleeRK4BiC grid(clow, chigh, c_ratio, mu, h, graph, 100, 0.00001, 1.);
    grid.seed(grid_length * grid_length / 2. + grid_length / 2. );
    for(int i=0; i<steps; ++i)
    {
        out.open(mylib::tapeFileName("./data/snap/temp/", ".txt", i));
        grid.oneTimeStep();
        for(int i=0; i<grid.net_size; ++i)
        {
            out << grid.net.getNodeFromId(i).getNode().u << " ";
            if((i+1) % grid_length == 0)
                out << std::endl;
        }
        out.close();
    }
    cout << grid.getOcupition() << endl;


    // for time series

//    PUNGraph graph = TSnap::GenRndDegK(size, degree);
//    for(int i=0; i<repeat; ++i)
//    {
//        AlleeRK4BiC rk4(clow, chigh, c_ratio, mu, h, graph, 100, 0.00001, 1.);
//        rk4.seed();
//        rk4.sim(steps);
//        out << rk4.getOcupition() << endl;
//        cout << rk4.getOcupition() << endl;
//        cout << steps << endl;
//        for(int ii=0; ii<steps; ++ii)
//        {
//            cout << rk4.getOcupition() << endl;
//            rk4.oneTimeStep();
//        }

//    }

//    out.close();


    return 0;
}
