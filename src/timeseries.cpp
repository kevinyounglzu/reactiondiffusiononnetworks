#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

//    double k = parser.getDouble("k");
    double h = parser.getDouble("h");
    double mu = parser.getDouble("mu");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    PUNGraph graph = TSnap::GenRndDegK(size, degree);

    ofstream out;
    out.open(mylib::tapeFileName("./data/timeseries/", ".txt", argv[1]));

//    out << "# r=" << r << endl;
//    out << "# m=" << m << endl;
//    out << "# k=" << degree << endl;

//    for(double & k: mylib::linspace(0, 0.3, 50))
//    {
//        out << k << " ";
//        AlleeRK4 rk4(r, k, m, h, graph, 50, 0.00001, 1.);
//        rk4.sim(steps, out);
//    }
    
    // repeat the timeseries
//    for(int i=0; i<100; ++i)
//    {
//        double k(0.03);
//        out << k << " ";
//        AlleeRK4 rk4(r, k, m, h, graph, 50, 0.00001, 1.);
//        rk4.sim(steps, out);
//        
//    }
    // for timeseries
//    double k = parser.getDouble("k");
//    double u = parser.getDouble("u");
//    AlleeRK4 rk4(k, mu, h, graph, 50, 0.00001, 1.);
//    rk4.getRandomNode().u = u;
//    rk4.dist(steps, out);

    double k = parser.getDouble("k");
    for(double & mu: mylib::linspace(1, 20, 19))
    {
        for(double & h: mylib::linspace(0.01, 1 / mu, 10))
        {
            AlleeRK4 rk4(k, mu, h, graph, 50, 0.00001, 1.);
            out << mu << " " << h << " ";
            int flag(1);
            for(int i=0; i<10; ++i)
            {
                rk4.oneTimeStep();
                if(!rk4.guard())
                {
                    flag = 0;
                    break;
                }
            }
            out << flag << endl;
        }
    }

    out.close();


    return 0;
}
