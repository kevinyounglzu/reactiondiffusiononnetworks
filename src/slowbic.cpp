#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    double clow = parser.getDouble("clow");
    double chigh = parser.getDouble("chigh");
    double c_ratio = parser.getDouble("cratio");

    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/slowbic/", ".txt", argv[1]));

    out << "# l=" << clow << endl;
    out << "# h=" << chigh << endl;
    out << "# r=" << c_ratio << endl;
    out << "# m=" << mu << endl;
    out << "# s=" << size << endl;
    out << "# d=" << degree << endl; 


    // for distribution
    
//    PUNGraph graph = TSnap::GenRndDegK(size, degree);
//    AlleeRK4BiC rk4(clow, chigh, c_ratio, mu, h, graph, 100, 0.00001, 1.);
//    for(int i=0; i<size; ++i)
//    {
//        cout << rk4.getNode(i).c << endl;;
//    }
//    rk4.seed();
//    for(int i=0; i<steps; ++i)
//    {
//        rk4.oneTimeStep();
//        if (rk4.swc.pushValue(rk4.getOcupition()))
//            break;
//        cout << rk4.getOcupition() << endl;
//    }
//    rk4.sim(steps, cout);

    // for time series

    PUNGraph graph = TSnap::GenRndDegK(size, degree);
    for(int i=0; i<repeat; ++i)
    {
        AlleeRK4BiC rk4(clow, chigh, c_ratio, mu, h, graph, 100, 0.00001, 1.);
        rk4.seed();
        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
//        cout << rk4.getOcupition() << endl;
//        cout << steps << endl;
//        for(int ii=0; ii<steps; ++ii)
//        {
//            cout << rk4.getOcupition() << endl;
//            rk4.oneTimeStep();
//        }

    }

    out.close();


    return 0;
}
