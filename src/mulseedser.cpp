#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"
#include "utils.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;
using mylib::operator<<;

void seedN(AlleeRK4 & rk4, int seed)
{
    vector<int> vec = mylib::range(rk4.net_size);
    vector<int> rands;

    mylib::RandInt ri(10);
    int max_index = rk4.net_size - 1;

    for(int i=0; i<seed; ++i)
    {
        int index = ri.genOTF(0, max_index);
        swap(vec[index], vec[max_index]);
        rands.push_back(vec[max_index]);
        max_index -= 1;
    }

    for(int id : rands)
    {
        rk4.seed(id);
    }
}


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int seed = parser.getInt("seed");
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    double mu = parser.getDouble("mu");
    double c = parser.getDouble("c");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/mulseedser/", ".txt", argv[1]));

    out << "# c=" << c << endl;
    out << "# m=" << mu << endl;
    out << "# i=" << seed << endl;
    out << "# s=" << size << endl;
    out << "# d=" << degree << endl; 


    for(int i=0; i<repeat; ++i)
    {
        PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
        AlleeRK4 rk4(c, mu, h, graph, 50, 0.00001, 1.);
        seedN(rk4, seed);
        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
    }

    out.close();

    return 0;
}
