#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;

void seeding(AlleeRK4 & line, int seed)
{
    int initial = line.net_size / 2;
    for(int i=initial+1; i<initial+1+seed/2; ++i)
    {
        line.seed(i);
    }
    for(int i=initial; i>initial-seed/2; --i)
    {
        line.seed(i);
    }
}


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int grid_length = parser.getInt("l");
    int seed = parser.getInt("seed");
    double c = parser.getDouble("c");

    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);


    ofstream out;
    out.open(mylib::tapeFileName("./data/line/", ".txt", argv[1]));

    out << "# s=" << seed << endl;
    out << "# c=" << c << endl;
    out << "# m=" << mu << endl;
    out << "# l=" << grid_length << endl;


    // for checking
//    PUNGraph graph = TSnap::GenGrid<PUNGraph>(100, 1, false);
//    for(TUNGraph::TNodeI nodeiter = graph->BegNI(); nodeiter != graph->EndNI(); nodeiter++)
//    {
//        cout << nodeiter.GetId() << " ";
//        for(int i=0; i<nodeiter.GetDeg(); ++i)
//        {
//            cout << nodeiter.GetNbrNId(i) << " ";
//        }
//        cout << endl;
//    }

    // for time series
    PUNGraph graph = TSnap::GenGrid<PUNGraph>(grid_length, 1, false);
    AlleeRK4 line(c, mu, h, graph, 50, 0.00001, 1.);
    line.seed(grid_length/2);
    for(int i=0; i<steps; ++i)
    {
        line.oneTimeStep();

        for(AlleeRK4::NetType::NodeIterator node_iter = line.net.begin(); node_iter != line.net.end(); ++node_iter)
        {
            out << node_iter.getNode().u << " ";
        }
        out << endl;
    }

//     for swepp
//    PUNGraph graph = TSnap::GenGrid<PUNGraph>(grid_length, 1, false);
//    for(int i=0; i<repeat; ++i)
//    {
//        AlleeRK4 line(c, mu, h, graph, 50, 0.00001, 1.);
//        if(seed == 1)
//            line.seed(grid_length/2);
//        else
//            seeding(line, seed);
//        line.sim(steps);
//        out << line.getOcupition() << endl;
//    }

//    // check 
//    AlleeRK4 line(c, mu, h, graph, 50, 0.00001, 1.);
//    seeding(line, seed);
//
//    for(AlleeRK4::NetType::NodeIterator node_iter = line.net.begin(); node_iter != line.net.end(); ++node_iter)
//    {
//        cout << node_iter.getNode().u << " ";
//    }
//
//    cout << line.getOcupition() << endl;
//    out.close();


    return 0;
}
