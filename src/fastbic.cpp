#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    double clow = parser.getDouble("clow");
    double chigh = parser.getDouble("chigh");
    double c_ratio = parser.getDouble("cratio");

    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/fastbic/", ".txt", argv[1]));

    out << "# l=" << clow << endl;
    out << "# h=" << chigh << endl;
    out << "# r=" << c_ratio << endl;
    out << "# m=" << mu << endl;
    out << "# s=" << size << endl;
    out << "# d=" << degree << endl; 


    PUNGraph graph = TSnap::GenRndDegK(size, degree);
    for(int i=0; i<repeat; ++i)
    {
        bool flag(true);
        AlleeRK4BiC rk4(clow, chigh, c_ratio, mu, h, graph, 100, 0.00001, 1.);
        rk4.seed();

        for(int i=0; i<steps; ++i)
        {
            flag = true;
            rk4.oneTimeStep();
            double ocupition = rk4.getOcupition();
//            cout << i << " " << ocupition << endl;
            if(ocupition < 0.5)
            {
                out << 0 << endl;
                flag = false;
                break;
            }
            if(ocupition > 1.5)
            {
                out << size << endl;
                flag = false;
                break;
            }
        }
        if(flag)
            out << rk4.getOcupition() << endl;
    }

    out.close();


    return 0;
}
