#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;

void seeding(AlleeRK4Micro & net, double seedp)
{
    mylib::RandDouble rd;
    for(int i=0; i<net.net_size; ++i)
    {
        if(rd.gen() < seedp)
        {
            net.seed(i);
        }
    }
}

int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    double p = parser.getDouble("p");
    double c = parser.getDouble("c");
    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    ofstream outInflux;
    ofstream outAJ;

    out.open(mylib::tapeFileName("./data/thresholderMicro/", ".txt", argv[1]));
    outInflux.open(mylib::tapeFileName("./data/thresholderMicro/", "i.txt", argv[1]));
    outAJ.open("./data/thresholdMicroAJ.txt");

//    out << "# s=" << size << endl;
//    out << "# p=" << p << endl;
//    out << "# c=" << c << endl;
//    out << "# m=" << mu << endl;
//    out << "# d=" << degree << endl;


    for(int i=0; i<repeat; ++i)
    {
        PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
        AlleeRK4Micro rk4(c, mu, h, graph, 50, 0.00001, 1.);
        rk4.writeAJ(outAJ);
        seeding(rk4, p);
//        rk4.sim(steps);
        for(int j=0; j<steps; ++j)
        {
            rk4.writeDist(out);
            rk4.writeInflux(outInflux);
            rk4.oneTimeStep();
        }
//        rk4.dist(steps, out);
        cout << rk4.getOcupition() << endl;
//        out << rk4.getOcupition() << endl;
    }

    out.close();
    outInflux.close();
    outAJ.close();


    return 0;
}
