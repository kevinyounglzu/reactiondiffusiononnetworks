#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));
    // for graph
    int sons = parser.getInt("sons");
    int level = parser.getInt("level");
    PUNGraph graph = TSnap::GenTree<PUNGraph>(sons, level, false);

    double mu = parser.getDouble("mu");
    double k = parser.getDouble("k");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/tree/", ".txt", argv[1]));

    out << "# m=" << mu << endl;
    out << "# k=" << k << endl;
    out << "# d=" << sons << endl;
    out << "# l=" << level << endl;

    for(int i=0; i<repeat; ++i)
    {
        AlleeRK4 rk4(k, mu, h, graph, 50, 0.00001, 1.);
        rk4.seed(0);
        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
    }
    
    out.close();


    return 0;
}
