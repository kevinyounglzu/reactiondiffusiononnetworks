#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    double mu = parser.getDouble("mu");
    double k = parser.getDouble("k");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/slowersweep/", ".txt", argv[1]));

//    out << "# m=" << mu << endl;
    out << "# k=" << k << endl;
    out << "# m=" << mu << endl;
    // for 2
    out << "# s=" << size << endl;
    // for 1
    out << "# d=" << degree << endl; 


    // for time series
    
//    AlleeRK4 rk4(k, mu, h, graph, 50, 0.00001, 1.);
//    rk4.seed();
//    rk4.sim(steps, out);

    // for time series

    for(int i=0; i<repeat; ++i)
    {
        PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
        AlleeRK4 rk4(k, mu, h, graph, 50, 0.00001, 1.);
        rk4.seed();
        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
    }

    out.close();


    return 0;
}
