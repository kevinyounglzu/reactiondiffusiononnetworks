#include <iostream>
#include <vector>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
//    PUNGraph graph = GenStar(5);
//    for(int i=0; i<5; ++i)
//    {
//        auto node_iter = graph->GetNI(i);
//        cout << node_iter.GetId() << " " << node_iter.GetDeg() << endl;
//    }

//    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

//    double mu = parser.getDouble("mu");
//    double c = parser.getDouble("c");
//    double h = parser.getDouble("h");
//
//    double times = parser.getDouble("times");

    vector<double> cs = mylib::linspace(0, 1, 100);
    vector<double> mus = mylib::linspace(0, 2, 200);
    double h(0.5);
    double times(2000.);
    int steps = static_cast<int>(times / h);

    ofstream out;
//    out.open(mylib::tapeFileName("./data/maxc/", ".txt", argv[1]));
    out.open("./data/smallnet4.txt");
//
//    out << "# m=" << mu << endl;
//    out << "# c=" << c << endl;
//    out << "# d=" << degree << endl;
//
////    for(int i=0; i<repeat; ++i)
////    {

    // 3
//    PUNGraph graph = PUNGraph::New();
//    graph->AddNode(0);
//    graph->AddNode(1);
//    graph->AddNode(2);
//    graph->AddEdge(0, 1);
//    graph->AddEdge(1, 2);
//    graph->AddEdge(0, 2);

    // 4
    PUNGraph graph = PUNGraph::New();
    graph->AddNode(0);
    graph->AddNode(1);
    graph->AddNode(2);
    graph->AddNode(3);
    graph->AddEdge(0, 1);
    graph->AddEdge(1, 2);
    graph->AddEdge(2, 3);
    graph->AddEdge(0, 3);

//    PUNGraph graph = GenStar(3);
    for(double mu: mus)
    {
        for(double c: cs)
        {
            cout << mu << " " << c << endl;
            AlleeRK4 rk4(c, mu, h, graph, 50, 0.00001, 1.);
            rk4.seed(0);

            rk4.sim(steps);

            out << mu << " " << c << " " << rk4.getOcupition() << endl;
        }
    }

    out.close();


    return 0;
}
