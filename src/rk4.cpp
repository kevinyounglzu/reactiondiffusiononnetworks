#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main()
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", 1));
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    double r = parser.getDouble("r");
    double k = parser.getDouble("k");
    double h = parser.getDouble("h");
    double m = parser.getDouble("m");

    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open("./data/rk4.txt");

    PUNGraph graph = TSnap::GenRndDegK(size, degree);

    AlleeRK4 rk4(r, k, m, h, graph, 50, 0.000001, 1.);
    rk4.sim(steps, out, true);
    
    out.close();


    return 0;
}
