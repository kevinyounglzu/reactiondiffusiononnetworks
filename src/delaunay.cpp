#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    // for test graph

//    for(TUNGraph::TNodeI node_iter = graph->BegNI(); node_iter != graph->EndNI(); node_iter++)
//    {
//        cout << node_iter.GetId() << " " << node_iter.GetDeg() << endl;
//    }

//    for(int i=0; i<800; ++i)
//    {
//        auto node_iter = graph->GetNI(i);
//        cout << i << " " << node_iter.GetId() << " " << node_iter.GetDeg() << endl;
//    }



    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph

    double mu = parser.getDouble("mu");
    double c = parser.getDouble("c");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;

    PUNGraph graph = TSnap::LoadEdgeList<PUNGraph>("./netdata/delaunay800.dat");

    out.open(mylib::tapeFileName("./data/delaunay/", ".txt", argv[1]));

    out << "# c=" << c << endl;
    out << "# m=" << mu << endl;


    // for time series
    
//    AlleeRK4 rk4(k, mu, h, graph, 50, 0.00001, 1.);
//    rk4.seed();
//    rk4.sim(steps, out);

    // for time series

    for(int i=0; i<repeat; ++i)
    {
        AlleeRK4 rk4(c, mu, h, graph, 50, 0.00001, 1.);
        rk4.seed();
        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
    }

    out.close();


    return 0;
}
