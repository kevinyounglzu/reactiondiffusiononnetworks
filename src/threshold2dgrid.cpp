#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;

void seeding(AlleeRK4 & net, double seedp)
{
    mylib::RandDouble rd;
    for(int i=0; i<net.net_size; ++i)
    {
        if(rd.gen() < seedp)
        {
            net.seed(i);
        }
    }
}

int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int grid_length = parser.getInt("l");

    double p = parser.getDouble("p");
    double c = parser.getDouble("c");
    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/threshold2gridSection/", ".txt", argv[1]));

    out << "# s=" << grid_length << endl;
    out << "# p=" << p << endl;
    out << "# c=" << c << endl;
    out << "# m=" << mu << endl;

    PUNGraph graph = TSnap::GenGrid<PUNGraph>(grid_length, grid_length, false);

    for(int i=0; i<repeat; ++i)
    {
        AlleeRK4 rk4(c, mu, h, graph, 50, 0.00001, 1.);
        seeding(rk4, p);
        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
    }

    out.close();


    return 0;
}
