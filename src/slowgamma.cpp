#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");
    double sigma = parser.getDouble("sigma");

    double mu = parser.getDouble("mu");
    double c = parser.getDouble("c");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/slowgammasweep/", ".txt", argv[1]));

    out << "# c=" << c << endl;
    out << "# m=" << mu << endl;
    out << "# s=" << size << endl;
    out << "# d=" << degree << endl; 
    out << "# f=" << sigma << endl;


    // for time series
    
//    PUNGraph graph = GenGamma(size, degree, sigma);
//    AlleeRK4 rk4(c, mu, h, graph, 100, 0.00001, 1.);
//    rk4.seed();
//    for(int i=0; i<steps; ++i)
//    {
//        rk4.oneTimeStep();
//        if (rk4.swc.pushValue(rk4.getOcupition()))
//            break;
//        cout << rk4.getOcupition() << endl;
//    }
//    rk4.sim(steps, cout);

    // for time series

    for(int i=0; i<repeat; ++i)
    {
        PUNGraph graph = GenGamma(size, degree, sigma);
        AlleeRK4 rk4(c, mu, h, graph, 50, 0.00001, 1.);
        rk4.seed();
        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
    }

    out.close();


    return 0;
}
