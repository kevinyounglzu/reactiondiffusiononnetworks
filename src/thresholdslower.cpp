#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;

void seeding(AlleeRK4 & net, double seedp)
{
    mylib::RandDouble rd;
    for(int i=0; i<net.net_size; ++i)
    {
        if(rd.gen() < seedp)
        {
            net.seed(i);
        }
    }
}

int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    double p = parser.getDouble("p");
    double c = parser.getDouble("c");
    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/thresholderSectionVaryC/", ".txt", argv[1]));

    out << "# s=" << size << endl;
    out << "# p=" << p << endl;
    out << "# c=" << c << endl;
    out << "# m=" << mu << endl;
    out << "# d=" << degree << endl;

    ofstream outI;
    outI.open(mylib::tapeFileName("./data/thresholderSectionVarySizeInt/", ".txt", argv[1]));

    outI << "# s=" << size << endl;
    outI << "# p=" << p << endl;
    outI << "# c=" << c << endl;
    outI << "# m=" << mu << endl;
    outI << "# d=" << degree << endl;


    // for time series
    
//    AlleeRK4 rk4(k, mu, h, graph, 50, 0.00001, 1.);
//    rk4.seed();
//    rk4.sim(steps, out);

    // for time series

    for(int i=0; i<repeat; ++i)
    {
        PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
        AlleeRK4 rk4(c, mu, h, graph, 50, 0.00001, 1.);
        seeding(rk4, p);
        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
        outI << rk4.getIntOcupition() << endl;
    }

    out.close();
    outI.close();


    return 0;
}
