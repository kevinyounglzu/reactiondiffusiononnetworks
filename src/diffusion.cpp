#include <iostream>
#include <cmath>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");
    int repeat = parser.getInt("repeat");

    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    double target = 1 / static_cast<double>(size);

    double margin = parser.getDouble("margin");

    ofstream out;
    out.open(mylib::tapeFileName("./data/diffusion/", ".txt", argv[1]));
    
    out << "# s=" << size << endl;
    out << "# d=" << degree << endl;
    out << "# m=" << mu << endl;
    out << "# g=" << margin << endl;


//    out << "# l=" << grid_length << endl;
//    out << "# c=" << c << endl;
//    out << "# m=" << mu << endl;


//    PUNGraph graph = TSnap::GenGrid<PUNGraph>(100, 1, false);

    // for time series
    PUNGraph graph = TSnap::GenRndDegK(size, degree);
    for(int j=0; j<repeat; ++j)
    {
        
        AlleeRK4Diffusion rk4d(mu, h, graph, 50, 0.00001, 1);
        rk4d.seed();
        for(int i=0; i<steps; ++i)
        {

            double base(0);
            for(AlleeRK4::NetType::NodeIterator node_iter = rk4d.net.begin(); node_iter != rk4d.net.end(); ++node_iter)
            {
                base += abs(node_iter.getNode().u - target);
    //            out << node_iter.getNode().u << " ";
            }
    //        out << i * h << " " << base * target << endl;
            if(base * target < margin)
            {
                out << i * h << endl;
                break;
            }

            rk4d.oneTimeStep();
        }


    }
    out.close();


    return 0;
}
