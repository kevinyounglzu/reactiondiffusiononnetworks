#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int grid_length = parser.getInt("l");
    double c = parser.getDouble("c");

    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");
    double threshold = parser.getDouble("t");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);


    ofstream out;
    out.open(mylib::tapeFileName("./data/line/", ".txt", argv[1]));

    out << "# l=" << grid_length << endl;
    out << "# c=" << c << endl;
    out << "# m=" << mu << endl;


    // for checking
//    PUNGraph graph = TSnap::GenGrid<PUNGraph>(100, 1, false);
//    for(TUNGraph::TNodeI nodeiter = graph->BegNI(); nodeiter != graph->EndNI(); nodeiter++)
//    {
//        cout << nodeiter.GetId() << " ";
//        for(int i=0; i<nodeiter.GetDeg(); ++i)
//        {
//            cout << nodeiter.GetNbrNId(i) << " ";
//        }
//        cout << endl;
//    }

    // for time series
    PUNGraph graph = TSnap::GenGrid<PUNGraph>(grid_length, 1, false);
    // for regular
    AlleeRK4 line(c, mu, h, graph, 50, 0.00001, 1.);
    // seed all the first half
//    line.seed(grid_length/2);
    for(AlleeRK4::NetType::NodeIterator node_iter = line.net.begin(); node_iter != line.net.getNodeFromId(grid_length/2); ++node_iter)
    {
        node_iter.getNode().u = 1;
    }

//     start to simulate for speed
    for(int i=0; i<steps; ++i)
    {
        line.oneTimeStep();
        line.getNode(0).u = 1;

        if(i * h <200)
            continue;
        else
            {
                if(line.getOcupition() < 0.9)
                    break;
            }

        for(AlleeRK4::NetType::NodeIterator node_iter = line.net.begin(); node_iter != line.net.end(); ++node_iter)
        {

            if(node_iter.getNode().u < threshold)
            {
                out << i * h << " " << node_iter->GetId() << endl;
                break;
            }
        }
    }


    // for swepp
//    for(int i=0; i<steps; ++i)
//    {
//        line.oneTimeStep();
//        line.getNode(0).u = 1;
//        for(AlleeRK4::NetType::NodeIterator node_iter = line.net.begin(); node_iter != line.net.end(); ++node_iter)
//        {
//            out << node_iter.getNode().u << " ";
//        }
//        out << endl;
//    }

    out.close();


    return 0;
}
