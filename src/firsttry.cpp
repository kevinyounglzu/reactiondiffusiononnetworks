#include <iostream>
#include <string>

#include "rand.h"
#include "parser.h"
#include "utils.h"
#include "mymath.h"

#include "Snap.h"

#include "snaphelper.h"
#include "patch.h"
//#include "wave.h"
#include "alleediffusion.h"

using namespace std;
using mylib::operator<<;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    int size = parser.getInt("size");
    int agent_n = parser.getInt("agent_n");
    int steps = parser.getInt("steps");
    int degree = parser.getInt("degree");
////    double f0 = parser.getDouble("f0");
    double r = parser.getDouble("r");
//    double k = parser.getDouble("k");
    double m = parser.getDouble("m");

//    cout << size << endl;
//    cout << agent_n << endl;
    ofstream out;

    PUNGraph graph = TSnap::GenRndDegK(size, degree);


    out.open(mylib::tapeFileName("./data/regular4/", ".txt", argv[1]));
    out << "# r=" << r << endl;
    out << "# m=" << m << endl;
    out << "# k=" << degree << endl;

    for(double & k: mylib::linspace(0, 0.3, 50))
    {
        ConstDiffuse cd(agent_n, r, k, m, graph);
        out << k << " ";
        cd.sim(steps, out);
//        cout << counter++ << endl;
    }

//    int counter(0);
//    for(double & r: mylib::linspace(0.0, 0.2, 10))
//    {
//        for(double & m: mylib::linspace(0.0, 0.2, 10))
//        {
//            for(double & k: mylib::linspace(0, 0.3, 10))
//            {
//                ConstDiffuse cd(agent_n, r, k, m, graph);
//                cd.sim(steps, out);
//                cout << counter++ << endl;
//            }
//        }
//    }
    out.close();


    return 0;
}
