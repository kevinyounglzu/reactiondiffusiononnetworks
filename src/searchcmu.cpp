#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;

double findC(PUNGraph graph, double upperbound, double lowerbound, int level, double mu, int steps, int repeat, int h, int size, double cutoff)
//{
//    return mu * (0.2-mu);
//}
{
    double inter(0);
    for(int j=0; j<level; ++j)
    {
        inter = (upperbound - lowerbound) / 2. + lowerbound;

        // find value for inter
        double base(0);
        for(int i=0; i<repeat; ++i)
        {
            // for regular
//            AlleeRK4 rk4(inter, mu, h, graph, 50, 0.00001, 1.);
            // for solid seed
            AlleeRK4SolidSeed rk4(inter, mu, h, graph, 50, 0.00001, 1.);
            rk4.seed();
            rk4.sim(steps);
            base += rk4.getOcupition();
        }
        double inter_value = base / static_cast<double>(repeat);
        if(inter_value < size * cutoff)
            upperbound = inter;
        else
            lowerbound = inter;

//        cout << j << " " << inter << endl;
    }

    return inter;

}


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));
//    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    int mulevel = parser.getInt("mulevel");
    int clevel = parser.getInt("clevel");

    PUNGraph graph = TSnap::GenRndDegK(size, degree);

    double upperbound = parser.getDouble("upper");
    double lowerbound = parser.getDouble("lower");

    double left_mu = parser.getDouble("leftmu");
    double right_mu = parser.getDouble("rightmu");

    double cutoff = parser.getDouble("cutoff");

    double h = parser.getDouble("h");
    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/searchcmu/", ".txt", argv[1]));

//    out << "# m=" << mu << endl;
//    out << "# c=" << c << endl;
    out << "# d=" << degree << endl;

//    double mu(0.12);

    
    
    double left_value = findC(graph, upperbound, lowerbound, clevel, left_mu, steps, repeat, h, size, cutoff);
    double right_value = findC(graph, upperbound, lowerbound, clevel, right_mu, steps, repeat, h, size, cutoff);

    double inter_mu_1(0);
    double inter_value_1(0);

    double inter_mu_2(0);
    double inter_value_2(0);

    double mid_mu(0);
    double mid_value(0);

    for(int i=0; i<mulevel; ++i)
    {
        inter_mu_1 = (right_mu - left_mu) / 3. + left_mu;
        inter_mu_2 = (right_mu - left_mu) / 3. * 2. + left_mu;
        inter_value_1 = findC(graph, upperbound, lowerbound, clevel, inter_mu_1, steps, repeat, h, size, cutoff);
        inter_value_2 = findC(graph, upperbound, lowerbound, clevel, inter_mu_2, steps, repeat, h, size, cutoff);

        cout << "Level: " << i << endl;

        out << i << endl;
        out << left_mu << " " << inter_mu_1 << " " << inter_mu_2 << " " << right_mu << endl;
        out << left_value << " " << inter_value_1 << " " << inter_value_2 << " " << right_value << endl;
        out << endl;

        if(inter_value_1 > left_value && inter_value_2 > inter_value_1)
        {
            left_mu = inter_mu_1;
            left_value = inter_value_1;

            mid_mu = inter_mu_2;
            mid_value = inter_value_2;
        }
        else
        {
            right_mu = inter_mu_2;
            right_value = inter_value_2;

            mid_mu = inter_mu_1;
            mid_value = inter_value_1;
        }


    }

    out << "Best: " << mid_mu << " " << mid_value << endl;
    cout << "Best: " << mid_mu << " " << mid_value << endl;
    out.close();


    return 0;
}
