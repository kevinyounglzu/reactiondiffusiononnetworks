#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"
#include "utils.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;
using mylib::operator<<;

void seeding(AlleeRK4 & line, int seed)
{
    int initial = line.net_size / 2;
    for(int i=initial+1; i<initial+1+seed/2; ++i)
    {
        line.seed(i);
    }
    for(int i=initial; i>initial-seed/2; --i)
    {
        line.seed(i);
    }
}


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int grid_length = parser.getInt("l");
    int seed = parser.getInt("seed");
    double c = parser.getDouble("c");

    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");

    double times = parser.getDouble("times");

    int steps = static_cast<int>(times / h);


    ofstream out;
    out.open(mylib::tapeFileName("./data/line/", ".txt", seed), std::ios_base::app);


    PUNGraph graph = TSnap::GenGrid<PUNGraph>(grid_length, 1, false);

    AlleeRK4 line(c, mu, h, graph, 50, 0.00001, 1.);
    if(seed == 1)
        line.seed(grid_length/2);
    else
        seeding(line, seed);

    line.sim(steps);
    out << mu << " " << c << " " << line.getOcupition() << endl;


    out.close();

    return 0;
}
