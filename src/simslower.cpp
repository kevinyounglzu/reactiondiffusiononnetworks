#include <iostream>
#include <string>

#include "rand.h"
#include "parser.h"
#include "utils.h"
#include "mymath.h"

#include "Snap.h"

#include "snaphelper.h"
#include "patch.h"
//#include "wave.h"
#include "alleediffusion.h"

using namespace std;
using mylib::operator<<;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    int agent_n = parser.getInt("agent_n");
    int steps = parser.getInt("steps");

    double r = parser.getDouble("r");
    double m = parser.getDouble("m");
    double c = parser.getDouble("c");

    int repeat = parser.getInt("repeat");

    ofstream out;



    out.open(mylib::tapeFileName("./data/simslower/", ".txt", argv[1]));
    out << "# s=" << size << endl;
    out << "# r=" << r << endl;
    out << "# m=" << m << endl;
    out << "# c=" << c << endl;
    out << "# d=" << degree << endl;

    double cutoff = 0.975 * size;
    for(int i=0; i<repeat; ++i)
    {
        bool flag(true);
        PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
        AlleeDiffusion ad(agent_n, r, c, m, 0.975, graph, 50, 0.1, 1.);
//        ad.sim(steps, cout);
        for(int i=0; i<steps; ++i)
        {
            flag = true;
            ad.oneMCStep();
            double ocupition = ad.getOcupition();
//            cout << i << " " << ocupition << endl;
            if(ocupition > cutoff)
            {
                out << ocupition << endl;
                flag = false;
                break;
            }
            if(ocupition < 0.5)
            {
                out << 0 << endl;
                flag = false;
                break;
            }
        }
        if(flag)
            out << ad.getOcupition() << endl;
    }

    out.close();


    return 0;
}
