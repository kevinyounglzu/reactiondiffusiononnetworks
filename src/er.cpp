#include <iostream>
#include <string>

#include "rand.h"
#include "parser.h"
#include "utils.h"
#include "mymath.h"

#include "Snap.h"

#include "snaphelper.h"
#include "patch.h"
//#include "wave.h"
#include "alleediffusion.h"

using namespace std;
using mylib::operator<<;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    int size = parser.getInt("size");
    int agent_n = parser.getInt("agent_n");
    int steps = parser.getInt("steps");
    int degree = parser.getInt("degree");
////    double f0 = parser.getDouble("f0");
    double r = parser.getDouble("r");
//    double k = parser.getDouble("k");
    double m = parser.getDouble("m");

    ofstream out;

//    PUNGraph er_graph = TSnap::GenRndGnm<PUNGraph>(size, static_cast<int>(size * (size-1)/2. * p), false);
    PUNGraph graph = TSnap::GenRndGnm<PUNGraph>(size, size * degree / 2, false);
//    profileNetworks(er_graph, "myer");


    out.open(mylib::tapeFileName("./data/er/", ".txt", argv[1]));
    out << "# r=" << r << endl;
    out << "# m=" << m << endl;
    out << "# k=" << degree << endl;

    for(double & k: mylib::linspace(0, 0.3, 50))
    {
        ConstDiffuse cd(agent_n, r, k, m, graph);
        out << k << " ";
        cd.sim(steps, out);
    }
    out.close();


    return 0;
}
