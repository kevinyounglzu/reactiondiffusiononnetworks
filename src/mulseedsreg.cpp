#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;

void printNodeInfor(AlleeRK4::NetType::NodeIterator & the_node)
{
    cout << the_node->GetId() << ":";
    for(int i=0; i<the_node->GetDeg(); ++i)
    {
        cout << the_node->GetNbrNId(i) << " ";
    }
    cout << endl;
}

void randomSeeding(AlleeRK4 & rk4)
{
    // try to find one seed randomly
    int first_id = rk4.rand_patch.gen();
    int second_id(0);
    AlleeRK4::NetType::NodeIterator first_node = rk4.net.getNodeFromId(first_id);
    while(true)
    {
        second_id = rk4.rand_patch.gen();
        if(!first_node->IsNbrNId(second_id))
            break;
    }

    rk4.seed(first_id);
    rk4.seed(second_id);

//    cout << first_node->GetId() << ":";
//    for(int i=0; i<first_node->GetDeg(); ++i)
//    {
//        cout << first_node->GetNbrNId(i) << " ";
//    }
//    cout << endl;
//
//    AlleeRK4::NetType::NodeIterator second_node = rk4.net.getNodeFromId(second_id);
//
//    cout << second_id << ":";
//
//    for(int i=0; i<second_node->GetDeg(); ++i)
//    {
//        cout << second_node->GetNbrNId(i) << " ";
//    }
//    cout << endl;
}

void sequelSeeding2(AlleeRK4 & rk4)
{
    int first_id = rk4.rand_patch.gen();
    AlleeRK4::NetType::NodeIterator first_node = rk4.net.getNodeFromId(first_id);
    int second_id = first_node->GetNbrNId(0);

    rk4.seed(first_id);
    rk4.seed(second_id);
}

void sequelSeeding3(AlleeRK4 & rk4)
{
    int first_id = rk4.rand_patch.gen();
    AlleeRK4::NetType::NodeIterator first_node = rk4.net.getNodeFromId(first_id);

    int second_id = first_node->GetNbrNId(0);
    AlleeRK4::NetType::NodeIterator second_node = rk4.net.getNodeFromId(second_id);

    int third_id(0);
    for(int i=0; i<second_node->GetDeg(); ++i)
    {
        third_id = second_node->GetNbrNId(i);
        if(third_id!=first_id)
            break;
    }

    rk4.seed(first_id);
    rk4.seed(second_id);
    rk4.seed(third_id);


//    AlleeRK4::NetType::NodeIterator third_node = rk4.net.getNodeFromId(third_id);
//
//    printNodeInfor(first_node);
//    printNodeInfor(second_node);
//    printNodeInfor(third_node);
//
}



int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));
    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");
    PUNGraph graph = TSnap::GenRndDegK(size, degree);

    double mu = parser.getDouble("mu");
    double c = parser.getDouble("c");
    double h = parser.getDouble("h");
//    if(mu > 0.8)
//        h = 0.8 / mu;

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/mulseedsreg/", ".txt", argv[1]));

    out << "# m=" << mu << endl;
    out << "# c=" << c << endl;
    out << "# d=" << degree << endl;

    for(int i=0; i<repeat; ++i)
    {
        AlleeRK4 rk4(c, mu, h, graph, 50, 0.00001, 1.);

        // for random seeding
//        randomSeeding(rk4);

        // for sequal seeding
//        sequelSeeding2(rk4);
        sequelSeeding3(rk4);

        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
    }


//    AlleeRK4 rk4(c, mu, h, graph, 50, 0.00001, 1.);
//    sequelSeeding2(rk4);
//    cout << rk4.getOcupition() << endl;

    out.close();


    return 0;
}
