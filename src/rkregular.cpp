#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));
    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");
    PUNGraph graph = TSnap::GenRndDegK(size, degree);

    double mu = parser.getDouble("mu");
    double c = parser.getDouble("c");
    double h = parser.getDouble("h");
//    if(mu > 0.8)
//        h = 0.8 / mu;

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/maxc/", ".txt", argv[1]));

    out << "# m=" << mu << endl;
    out << "# c=" << c << endl;
    out << "# d=" << degree << endl;

    for(int i=0; i<repeat; ++i)
    {
        AlleeRK4 rk4(c, mu, h, graph, 50, 0.00001, 1.);
        rk4.seed();
        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
    }
//    for(int i=0; i<repeat; ++i)
//    {
//        bool flag(true);
//        AlleeRK4 rk4(c, mu, h, graph, 50, 0.001, 1.);
//        rk4.seed();
//        for(int i=0; i<steps; ++i)
//        {
//            flag = true;
//            rk4.oneTimeStep();
//            double ocupition = rk4.getOcupition();
//            if(rk4.swc.pushValue(ocupition))
//            {
//                break;
//            }
//            if(ocupition < 0.7)
//            {
//                out << 0 << endl;
//                flag = false;
//                break;
//            }
//            if(ocupition > degree)
//            {
//                out << size << endl;
//                flag = false;
//                break;
//            }
//        }
//        if(flag)
//            out << rk4.getOcupition() << endl;
//    }
//        rk4.sim(steps, out);
    
    out.close();


    return 0;
}
