#include <iostream>
#include <string>

#include "rand.h"
#include "parser.h"
#include "utils.h"
#include "mymath.h"

#include "Snap.h"

#include "snaphelper.h"
#include "patch.h"
//#include "wave.h"
#include "alleediffusion.h"

using namespace std;
using mylib::operator<<;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    int size = parser.getInt("size");
    int degree = parser.getInt("degree");
    double sigma = parser.getDouble("sigma");

    int agent_n = parser.getInt("agent_n");
    int steps = parser.getInt("steps");

    double r = parser.getDouble("r");
    double m = parser.getDouble("m");
    double c = parser.getDouble("c");

    int repeat = parser.getInt("repeat");

    ofstream out;

    out.open(mylib::tapeFileName("./data/simfastgamma/", ".txt", argv[1]));
    out << "# s=" << size << endl;
    out << "# d=" << degree << endl;
    out << "# r=" << r << endl;
    out << "# m=" << m << endl;
    out << "# c=" << c << endl;
    out << "# f=" << sigma << endl;

    for(int i=0; i<repeat; ++i)
    {
        bool flag(true);
        PUNGraph graph = GenGamma(size, degree, sigma);
        AlleeDiffusion ad(agent_n, r, c, m, 0.01, graph, 50, 0.00001, 1.);
        for(int i=0; i<steps; ++i)
        {
            flag = true;
            ad.oneMCStep();
            double ocupition = ad.getOcupition();
//            cout << i << " " << ocupition << endl;
            if(ocupition < 0.95)
            {
                out << 0 << endl;
                flag = false;
                break;
            }
            if(ocupition > 1.1)
            {
                out << size << endl;
                flag = false;
                break;
            }
        }
        if(flag)
        {
            out << ad.getOcupition() << endl;
        }
    }

    out.close();


    return 0;
}
