#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));
    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");
    PUNGraph graph = TSnap::GenRndDegK(size, degree);

    double mu = parser.getDouble("mu");
    double k = parser.getDouble("k");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);

    ofstream out;
    out.open(mylib::tapeFileName("./data/fast/", ".txt", argv[1]));

    out << "# s=" << size << endl;
    out << "# m=" << mu << endl;
    out << "# k=" << k << endl;

    for(int i=0; i<repeat; ++i)
    {
        AlleeRK4 rk4(k, mu, h, graph, 50, 0.00001, 1.);
        rk4.seed();
        rk4.sim(steps);
        out << rk4.getOcupition() << endl;
    }
//        AlleeRK4 rk4(k, mu, h, graph, 50, 0.00001, 1.);
//        rk4.seed();
//        rk4.dist(steps, out);
//        rk4.sim(steps, out);
        
    out.close();


    return 0;
}
