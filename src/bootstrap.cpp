#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;

void seeding(AlleeRK4 & net, double seedp)
{
    mylib::RandDouble rd;
    for(int i=0; i<net.net_size; ++i)
    {
        if(rd.gen() < seedp)
        {
            net.seed(i);
        }
    }
}


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    // for graph
    int size = parser.getInt("size");
    int degree = parser.getInt("degree");

    double p = parser.getDouble("p");
    double c = parser.getDouble("c");
    double mu = parser.getDouble("mu");
    double h = parser.getDouble("h");

    int repeat = parser.getInt("repeat");
    double times = parser.getDouble("times");
    int steps = static_cast<int>(times / h);


    ofstream out;
    out.open(mylib::tapeFileName("./data/bootstrap/", ".txt", argv[1]));

    out << "# c=" << c << endl;
    out << "# p=" << p << endl;
    out << "# s=" << size << endl;
    out << "# m=" << mu << endl;
    out << "# d=" << degree << endl;

    PUNGraph graph = TSnap::GenRndDegK(size, degree);
    // for long run
    for(int i=0; i<repeat; ++i)
    {
        AlleeRK4 local(c, mu, h, graph, 50, 0.00001, 1.);
        seeding(local, p);
        local.sim(steps);
        out << local.getOcupition() << endl;
    }

    // initialize
//    PUNGraph graph = TSnap::GenRndDegK(size, degree);
//    AlleeRK4 local(c, mu, h, graph, 50, 0.00001, 1.);
//    seeding(local, p);

//    for(int i=0; i<steps; ++i)
//    {
//        for(AlleeRK4::NetType::NodeIterator node_iter = local.net.begin(); node_iter != local.net.end(); ++node_iter)
//        {
//            out << node_iter.getNode().u << " ";
//        }
//        out << endl;
////        cout << local.getOcupition() << endl;
//        local.oneTimeStep();
//    }


    //    for checking
//    for(AlleeRK4::NetType::NodeIterator node_iter = local.net.begin(); node_iter != local.net.end(); ++node_iter)
//    {
//        cout << node_iter.getNode().u << " ";
//        int whole_degree = node_iter->GetDeg();
//        int counter(0);
//        for(int i=0; i<whole_degree; ++i)
//        {
//            cout << local.getNode(node_iter->GetNbrNId(i)).u << " ";
////            if(1 == local.getNode(node_iter->GetNbrNId(i)).u)
////                ++counter;
//        }
//        cout << endl;
//    }
//    cout << local.getOcupition() << endl;


    return 0;
}
