#include <iostream>

#include "rand.h"
#include "parser.h"
#include "mymath.h"

#include "Snap.h"

#include "alleediffusion.h"

using namespace std;


int main()
{
    

    int size = 1000;
    double dsize = static_cast<double>(size);
    int degree = 4;
    int upper_bound(100);
    int repeat(10000);
    double drepeat = static_cast<double>(repeat);

    ofstream out;

    PUNGraph graph = GenGamma(size, degree, 3);
    TIntPrV thevec;
    TSnap::GetSccSzCnt(graph, thevec);
    for(auto iter = thevec.BegI(); iter != thevec.EndI(); iter++)
    {
        cout << iter->GetVal1() << " " << iter->GetVal2() << endl;
    }

//    for(int sigma=1; sigma<5; ++sigma)
//    {
//        
//        out.open(mylib::tapeFileName("./data/gammaattack1000sigma", ".txt", sigma));
//
//        for(int i=0; i<upper_bound; ++i)
//        {
//            cout << i << endl;
//            double rest_base(0);
//            double largest_comp(0);
//            for(int j=0; j<repeat; ++j)
//            {
//                PUNGraph graph = GenGamma(size, degree, sigma);
//                attackAbove(graph, i);
//                rest_base += graph->GetNodes() / dsize;
//                largest_comp += graph->GetNodes() * TSnap::GetMxSccSz(graph) / dsize;
//            }
//            out << i << " " << rest_base / drepeat << " " << largest_comp / drepeat << endl;
//            
//        }

        out.close();

//    }

    return 0;
}
