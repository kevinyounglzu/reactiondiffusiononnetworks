#include <iostream>
#include <string>

#include "rand.h"
#include "parser.h"
#include "utils.h"
#include "mymath.h"

#include "Snap.h"

#include "snaphelper.h"
#include "patch.h"
//#include "wave.h"
#include "alleediffusion.h"

using namespace std;
using mylib::operator<<;


int main(int argc, char* argv[])
{
    mylib::Parser parser(mylib::tapeFileName("./config/", ".cfg", argv[1]));

    int size = parser.getInt("size");
    int degree = parser.getInt("degree");
    double sigma = parser.getDouble("sigma");

    int agent_n = parser.getInt("agent_n");

    double r = parser.getDouble("r");
    double mu = parser.getDouble("mu");
    double c = parser.getDouble("c");

    int repeat = parser.getInt("repeat");
    int steps = parser.getInt("steps");

    ofstream out;



    out.open(mylib::tapeFileName("./data/simslowgamma/", ".txt", argv[1]));
    out << "# s=" << size << endl;
    out << "# r=" << r << endl;
    out << "# m=" << mu << endl;
    out << "# c=" << c << endl;
    out << "# d=" << degree << endl;
    out << "# f=" << sigma << endl;

    double cutoff = 0.975 * size;
    for(int i=0; i<repeat; ++i)
    {
        bool flag(true);
        PUNGraph graph = GenGamma(size, degree, sigma);
        AlleeDiffusion ad(agent_n, r, c, mu, 0.975, graph, 50, 0.1, 1.);
//        ad.sim(steps, cout);
        for(int i=0; i<steps; ++i)
        {
            flag = true;
            ad.oneMCStep();
            double ocupition = ad.getOcupition();
//            cout << i << " " << ocupition << endl;
            if(ocupition > cutoff)
            {
                out << ocupition << endl;
                flag = false;
                break;
            }
            if(ocupition < 0.5)
            {
                out << 0 << endl;
                flag = false;
                break;
            }
        }
        if(flag)
            out << ad.getOcupition() << endl;
    }

    out.close();


    return 0;
}
