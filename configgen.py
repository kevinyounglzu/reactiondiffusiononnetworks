# -*- coding: utf8 -*-

import numpy as np


def genSimRegular():
    size = 300
    agent_n = 100
    degree = 6
    repeat = 1000
    steps = 1000
    # gen all configs

    cutoutp = 0.05

    r = 0.2

    mus = np.linspace(0, 0.25, 41)
    ms = mus * r

    cs = np.linspace(0., 1.0, 201)

    counter = 0
    for m in ms:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)
                f.write("int agent_n=%d\n" % agent_n)
                f.write("int steps=%d\n" % steps)
                f.write("double r=%f\n" % r)
                f.write("double m=%f\n" % m)
                f.write("double c=%f\n" % c)
                f.write("int repeat=%d\n" % repeat)
                f.write("double cutoutp=%f\n" % cutoutp)
                counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSimRegularCut():
    size = 300
    agent_ns = [500]
    degree = 6
    repeat = 400
    steps = 1000
    # gen all configs

    cutoutp = 0.05

    r = 0.2

    #mus = np.linspace(0, 0.25, 41)
    #ms = mus * r
    ms = [0.01]

    cs = np.linspace(0., 1.0, 201)

    counter = 0
    for m in ms:
        for agent_n in agent_ns:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int agent_n=%d\n" % agent_n)
                    f.write("int steps=%d\n" % steps)
                    f.write("double r=%f\n" % r)
                    f.write("double m=%f\n" % m)
                    f.write("double c=%f\n" % c)
                    f.write("int repeat=%d\n" % repeat)
                    f.write("double cutoutp=%f\n" % cutoutp)
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSimFastRegular():
    repeat = 1000
    steps = 1000
    # gen all configs

    agent_n = 100
    degree = 3

    sizes = [100, 48, 32, 24, 18, 16, 12, 10, 8]

    r = 0.05

    ms = [0.5]

    cs = np.linspace(0.005, 0.25, 101)

    counter = 0
    for size in sizes:
        for m in ms:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int agent_n=%d\n" % agent_n)
                    f.write("int steps=%d\n" % steps)
                    f.write("double r=%f\n" % r)
                    f.write("double m=%f\n" % m)
                    f.write("double c=%f\n" % c)
                    f.write("int repeat=%d\n" % repeat)
                    f.write("double cutoutp=%f\n" % (5 / float(size)))
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSimFastEr():
    repeat = 1000
    steps = 1000
    agent_n = 100
    r = 0.05
    m = 0.5

    # for c n relation
    degrees = [4]
    sizes = [450, 400, 350, 300, 250, 200, 150]
    cs = np.linspace(0.001, 0.01, 46)

    # for c lambda relation
    #degrees = range(4, 15)
    #sizes = [400]
    #cs = np.linspace(0.003, 0.005, 21)

    counter = 0
    for size in sizes:
        for degree in degrees:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int agent_n=%d\n" % agent_n)
                    f.write("int steps=%d\n" % steps)
                    f.write("double r=%f\n" % r)
                    f.write("double m=%f\n" % m)
                    f.write("double c=%f\n" % c)
                    f.write("int repeat=%d\n" % repeat)
                    f.write("double cutoutp=%f\n" % (5 / float(size)))
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSimFastErCNS():
    repeat = 100
    steps = 3000
    agent_n = 2000
    r = 0.09
    m = 0.9

    degree = 4

    configuration = [
        (150, [0.00782213, 0.00792213, 0.00802213, 0.00812213, 0.00822213, 0.00832213, 0.00842213, 0.00852213, 0.00862213, 0.00872213, 0.00882213]),
        (200, [0.00574371, 0.00584371, 0.00594371, 0.00604371, 0.00614371, 0.00624371, 0.00634371, 0.00644371, 0.00654371, 0.00664371, 0.00674371]),
        (250, [0.00449598, 0.00459598, 0.00469598, 0.00479598, 0.00489598, 0.00499598, 0.00509598, 0.00519598, 0.00529598, 0.00539598, 0.00549598]),
        (300, [0.00366388, 0.00376388, 0.00386388, 0.00396388, 0.00406388, 0.00416388, 0.00426388, 0.00436388, 0.00446388, 0.00456388, 0.00466388]),
        (350, [0.00306938, 0.00316938, 0.00326938, 0.00336938, 0.00346938, 0.00356938, 0.00366938, 0.00376938, 0.00386938, 0.00396938, 0.00406938]),
        (400, [0.00262343, 0.00272343, 0.00282343, 0.00292343, 0.00302343, 0.00312343, 0.00322343, 0.00332343, 0.00342343, 0.00352343, 0.00362343]),
        (450, [0.00227654, 0.00237654, 0.00247654, 0.00257654, 0.00267654, 0.00277654, 0.00287654, 0.00297654, 0.00307654, 0.00317654, 0.00327654])
    ]

    counter = 0
    for size, cs in configuration:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)
                f.write("int agent_n=%d\n" % agent_n)
                f.write("int steps=%d\n" % steps)
                f.write("double r=%f\n" % r)
                f.write("double m=%f\n" % m)
                f.write("double c=%f\n" % c)
                f.write("int repeat=%d\n" % repeat)
                #f.write("double cutoutp=%f\n" % (2 / float(size)))
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSimFastErCLambdaS():
    repeat = 100
    steps = 3000
    agent_n = 2000
    r = 0.09
    m = 0.9

    degree = 4

    size = 300

    configuration = [
        (4, [0.00386388, 0.00396388, 0.00406388, 0.00416388, 0.00426388, 0.00436388, 0.00446388, 0.00456388, 0.00466388, 0.00476388, 0.00486388]),
        (5, [0.00369777, 0.00379777, 0.00389777, 0.00399777, 0.00409777, 0.00419777, 0.00429777, 0.00439777, 0.00449777, 0.00459777, 0.00469777]),
        (6, [0.00358703, 0.00368703, 0.00378703, 0.00388703, 0.00398703, 0.00408703, 0.00418703, 0.00428703, 0.00438703, 0.00448703, 0.00458703]),
        (7, [0.00350793, 0.00360793, 0.00370793, 0.00380793, 0.00390793, 0.00400793, 0.00410793, 0.00420793, 0.00430793, 0.00440793, 0.00450793]),
        (8, [0.00344861, 0.00354861, 0.00364861, 0.00374861, 0.00384861, 0.00394861, 0.00404861, 0.00414861, 0.00424861, 0.00434861, 0.00444861]),
        (9, [0.00340246, 0.00350246, 0.00360246, 0.00370246, 0.00380246, 0.00390246, 0.00400246, 0.00410246, 0.00420246, 0.00430246, 0.00440246]),
        (10, [0.00336555, 0.00346555, 0.00356555, 0.00366555, 0.00376555, 0.00386555, 0.00396555, 0.00406555, 0.00416555, 0.00426555, 0.00436555]),
        (11, [0.00333535, 0.00343535, 0.00353535, 0.00363535, 0.00373535, 0.00383535, 0.00393535, 0.00403535, 0.00413535, 0.00423535, 0.00433535]),
        (12, [0.00331018, 0.00341018, 0.00351018, 0.00361018, 0.00371018, 0.00381018, 0.00391018, 0.00401018, 0.00411018, 0.00421018, 0.00431018]),
        (13, [0.00328889, 0.00338889, 0.00348889, 0.00358889, 0.00368889, 0.00378889, 0.00388889, 0.00398889, 0.00408889, 0.00418889, 0.00428889]),
        (14, [0.00327063, 0.00337063, 0.00347063, 0.00357063, 0.00367063, 0.00377063, 0.00387063, 0.00397063, 0.00407063, 0.00417063, 0.00427063])
    ]

    counter = 0
    for degree, cs in configuration:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)
                f.write("int agent_n=%d\n" % agent_n)
                f.write("int steps=%d\n" % steps)
                f.write("double r=%f\n" % r)
                f.write("double m=%f\n" % m)
                f.write("double c=%f\n" % c)
                f.write("int repeat=%d\n" % repeat)
                #f.write("double cutoutp=%f\n" % (2 / float(size)))
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSimFastGammaCSigma():
    size = 500
    agent_n = 2000
    degree = 4

    repeat = 100
    steps = 3000

    r = 0.09
    m = 0.9

    cs = np.linspace(0.001, 0.006, 101)

    configuration = [
        (1.0, [0.00210125, 0.00220125, 0.00230125, 0.00240125, 0.00250125, 0.00260125, 0.00270125, 0.00280125, 0.00290125, 0.00300125]),
        (1.5, [0.00198189, 0.00208189, 0.00218189, 0.00228189, 0.00238189, 0.00248189, 0.00258189, 0.00268189, 0.00278189, 0.00288189]),
        (2.0, [0.00220125, 0.00230125, 0.00240125, 0.00250125, 0.00260125, 0.00270125, 0.00280125, 0.00290125, 0.00300125, 0.00310125]),
        (2.5, [0.00220125, 0.00230125, 0.00240125, 0.00250125, 0.00260125, 0.00270125, 0.00280125, 0.00290125, 0.00300125, 0.00310125]),
        (3.0, [0.00282853, 0.00292853, 0.00302853, 0.00312853, 0.00322853, 0.00332853, 0.00342853, 0.00352853, 0.00362853, 0.00372853]),
        (3.5, [0.00323668, 0.00333668, 0.00343668, 0.00353668, 0.00363668, 0.00373668, 0.00383668, 0.00393668, 0.00403668, 0.00413668]),
        (4.0, [0.00360803, 0.00370803, 0.00380803, 0.00390803, 0.00400803, 0.00410803, 0.00420803, 0.00430803, 0.00440803, 0.00450803]),
        (4.5, [0.00384277, 0.00394277, 0.00404277, 0.00414277, 0.00424277, 0.00434277, 0.00444277, 0.00454277, 0.00464277, 0.00474277]),
        (5.0, [0.0043411, 0.0044411, 0.0045411, 0.0046411, 0.0047411, 0.0048411, 0.0049411, 0.0050411, 0.0051411, 0.0052411])
    ]

    counter = 0
    for sigma, cs in configuration:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int agent_n=%d\n" % agent_n)
                    f.write("double sigma=%f\n" % sigma)

                    f.write("double r=%f\n" % r)
                    f.write("double m=%f\n" % m)
                    f.write("double c=%f\n" % c)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("int steps=%d\n" % steps)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSimFastGammaCSize():
    agent_n = 2000
    degree = 4

    sigma = 1

    repeat = 100
    steps = 3000

    r = 0.09
    m = 0.9

    configuration = [
        (150, [0.00678631, 0.00688631, 0.00698631, 0.00708631, 0.00718631, 0.00728631, 0.00738631, 0.00748631, 0.00758631, 0.00768631]),
        (200, [0.00501417, 0.00511417, 0.00521417, 0.00531417, 0.00541417, 0.00551417, 0.00561417, 0.00571417, 0.00581417, 0.00591417]),
        (250, [0.00395107, 0.00405107, 0.00415107, 0.00425107, 0.00435107, 0.00445107, 0.00455107, 0.00465107, 0.00475107, 0.00485107]),
        (300, [0.00324241, 0.00334241, 0.00344241, 0.00354241, 0.00364241, 0.00374241, 0.00384241, 0.00394241, 0.00404241, 0.00414241]),
        (350, [0.00273626, 0.00283626, 0.00293626, 0.00303626, 0.00313626, 0.00323626, 0.00333626, 0.00343626, 0.00353626, 0.00363626]),
        (400, [0.00235667, 0.00245667, 0.00255667, 0.00265667, 0.00275667, 0.00285667, 0.00295667, 0.00305667, 0.00315667, 0.00325667]),
        (450, [0.00206144, 0.00216144, 0.00226144, 0.00236144, 0.00246144, 0.00256144, 0.00266144, 0.00276144, 0.00286144, 0.00296144])
    ]

    counter = 0
    for size, cs in configuration:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int agent_n=%d\n" % agent_n)
                    f.write("double sigma=%f\n" % sigma)

                    f.write("double r=%f\n" % r)
                    f.write("double m=%f\n" % m)
                    f.write("double c=%f\n" % c)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("int steps=%d\n" % steps)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4Regular():
    # gen all configs
    ms = np.linspace(0, 0.5, 51)
    rs = np.linspace(0, 0.6, 51)
    #ks = np.linspace(0, 0.3, 51)

    counter = 0
    for degree in [3, 4, 6]:
        for m in ms:
            for r in rs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=300\n")
                    f.write("int degree=%d\n" % degree)
                    f.write("double r=%f\n" % r)
                    f.write("double h=0.1\n")
                    f.write("double m=%f\n" % m)
                    f.write("int repeat=100\n")
                    f.write("double times=150.\n")
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4RegularNew():
    size = 300
    degree = 6
    h = 0.4
    repeat = 1000
    times = 20000.
    # gen all configs
    #mus = np.linspace(0, 0.25, 126)
    mus = np.linspace(0, 1, 101)
    #ks = np.linspace(0, 0.4, 81)
    #ks = np.linspace(0., 1.0, 201)
    # for 8
    # ks = np.linspace(0., 0.125, 26)
    ks = np.linspace(0., 0.17, 171)

    counter = 0
    for mu in mus:
        for k in ks:
            if mu > 0.5 and k > 0.01:
                break
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)

                f.write("double mu=%f\n" % mu)
                f.write("double k=%f\n" % k)
                f.write("double h=%f\n" % h)

                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genFindcmuMax():
    size = 300
    c_interval = 0.05
    mu_intervel = 0.03
    degrees = [6]

    h = 1
    repeat = 1000
    times = 2000.

    counter = 0

    for degree in degrees:
        max_c = 1 / float(degree)
        max_mu = 1 / float(degree) - 1 / float(degree * degree)
        cs = np.linspace(max_c - c_interval, max_c + c_interval, 101)
        mus = np.linspace(max_mu - mu_intervel, max_mu + mu_intervel, 31)
        for mu in mus:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double h=%f\n" % h)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSearchCMu():
    size = 300
    degrees = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

    cutoff = 0.9

    mulevel = 12
    clevel = 12

    h = 1
    repeat = 2000
    times = 2000.

    counter = 0

    for degree in degrees:

        max_c = 1 / float(degree)
        max_mu = 1 / float(degree) - 1 / float(degree**2)

        upper = max_c * 1.5
        lower = 0.

        left = 0.001
        if max_mu * 2 > 0.25:
            right = 0.25
        else:
            right = max_mu * 2

        filename = "./config/%d.cfg" % counter
        with open(filename, "w") as f:
            f.write("int size=%d\n" % size)
            f.write("int degree=%d\n" % degree)

            f.write("int clevel=%d\n" % clevel)
            f.write("int mulevel=%d\n" % mulevel)

            f.write("double upper=%f\n" % upper)
            f.write("double lower=%f\n" % lower)

            f.write("double leftmu=%f\n" % left)
            f.write("double rightmu=%f\n" % right)

            f.write("double cutoff=%f\n" % cutoff)

            f.write("double h=%f\n" % h)
            f.write("int repeat=%d\n" % repeat)
            f.write("double times=%f\n" % times)
            counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genTree():
    # gen all configs
    son_level = [(2, 8), (3, 5), (4, 4)]

    repeat = 3000
    times = 2000
    #mus = np.linspace(0, 10, 51)
    mus = [0.01]
    h = 1.
    ks = np.linspace(0, 0.3, 51)

    counter = 0
    for sons, level in son_level:
        for mu in mus:
            for k in ks:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int sons=%d\n" % sons)
                    f.write("int level=%d\n" % level)

                    f.write("double mu=%f\n" % mu)
                    f.write("double k=%f\n" % k)
                    f.write("double h=%f\n" % h)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genFast():
    #repeat = 1000
    repeat = 2000
    #times = 5000
    times = 20000
    degree = 3
    h = 0.05
    # gen all configs
    #mus = range(1, 16, 2)
    mus = [1, 2, 3]
    ks = np.linspace(0.005, 0.25, 101)
    #sizes = map(int, 1 / np.linspace(0.01, 0.12, 11))
    sizes = [100, 48, 32, 24, 18, 16, 12, 10, 8]

    counter = 0
    for size in sizes:
        for mu in mus:
            for k in ks:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)

                    f.write("double mu=%f\n" % mu)
                    f.write("double k=%f\n" % k)
                    f.write("double h=%f\n" % h)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genFastErCLambda():
    repeat = 2000
    times = 20000.
    size = 500
    mus = [10]
    h = 0.08
    # gen all configs
    ks = np.linspace(0.003, 0.005, 41)
    degrees = range(4, 15)

    counter = 0
    for mu in mus:
        for degree in degrees:
            for k in ks:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)

                    f.write("double mu=%f\n" % mu)
                    f.write("double k=%f\n" % k)
                    f.write("double h=%f\n" % h)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genFastErCN():
    repeat = 1000
    times = 2000.
    # gen all configs
    ks = np.linspace(0.001, 0.01, 91)
    #degrees = [3, 4, 6, 8]
    degree = 4
    sizes = [150, 200, 250, 300, 350, 400, 450]
    mus = range(1, 10)

    counter = 0
    for mu in mus:
        for size in sizes:
            for k in ks:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)

                    f.write("double mu=%d\n" % mu)
                    f.write("double k=%f\n" % k)
                    f.write("double h=0.08\n")

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSlowEr():
    size = 800
    degree = 6
# size = 500
# degree = 8

    h = 1.
    repeat = 3000
    times = 2000.
    #mus = np.linspace(0, 0.25, 126)
    #mus = [0.05, 0.1, 0.15, 0.2]
    #mus = [0.05, 0.138, 0.2]
    #mus = np.linspace(0, 1, 101)
    #ks = np.linspace(0.02, 0.15, 131)
    #ks = np.linspace(0.02, 0.3, 81)

    #ks = np.linspace(0., 1.0, 201)
    # to merge
    #repeat = 6000
    #mus = np.linspace(0.21, 0.25, 5)
    #ks = np.linspace(0.02, 0.2, 101)

    #mus = np.linspace(0, 0.25, 126)
    #ks = np.append(np.linspace(0.0, 0.2, 101), np.linspace(0.2, 1, 201)[1:])

    mus = np.linspace(0, 0.25, 126)
    ks = np.linspace(0, 0.3, 101)

    counter = 0
    for mu in mus:
        for k in ks:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)

                f.write("double mu=%f\n" % mu)
                f.write("double k=%f\n" % k)
                f.write("double h=%f\n" % h)

                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSimSlowEr():
    size = 400
    agent_n = 500
    degree = 6

    repeat = 100
    steps = 3000

    r = 0.2
    #mus = np.linspace(0, 0.25, 41)
    #ms = mus * r
    ms = [0.02]
    cs = np.linspace(0.02, 0.2, 51)

    counter = 0
    for i in range(10):
        for m in ms:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int agent_n=%d\n" % agent_n)

                    f.write("double r=%f\n" % r)
                    f.write("double m=%f\n" % m)
                    f.write("double c=%f\n" % c)
                    f.write("int repeat=%d\n" % repeat)
                    f.write("int steps=%d\n" % steps)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSlowDelaunay():
    h = 1.
    repeat = 3000
    times = 2000.

    # for sweep
    mus = np.linspace(0, 0.25, 126)
    cs = np.linspace(0, 0.3, 101)

    # for specific
    #mus = [0.05, 0.138, 0.2]
    #cs = np.linspace(0.0, 0.4, 101)

    counter = 0
    for mu in mus:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("double mu=%f\n" % mu)
                f.write("double c=%f\n" % c)
                f.write("double h=%f\n" % h)

                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSimSlowDelaunay():
    agent_n = 500
    repeat = 100
    steps = 3000
    r = 0.2
    ms = [0.01, 0.0276, 0.04]
    cs = np.linspace(0.0, 0.4, 101)

    counter = 0
    for m in ms:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int agent_n=%d\n" % agent_n)
                f.write("double r=%f\n" % r)
                f.write("double m=%f\n" % m)
                f.write("double c=%f\n" % c)

                f.write("int repeat=%d\n" % repeat)
                f.write("int steps=%d\n" % steps)
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSlowTri():
    h = 1.
    l = 28
    # for all
    #repeat = 3000

    # for speed
    repeat = 100
    times = 2000.

    # for sweep
    #mus = np.linspace(0, 0.25, 126)
    #cs = np.linspace(0, 0.3, 101)

    #for speed
    mus = np.linspace(0, 0.375, 101)
    cs = np.linspace(0, 0.3, 51)

    # for specific
    #mus = [0.05, 0.138, 0.2]
    #cs = np.linspace(0.0, 0.4, 101)

    counter = 0
    for mu in mus:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int l=%d\n" % l)
                f.write("double mu=%f\n" % mu)
                f.write("double c=%f\n" % c)
                f.write("double h=%f\n" % h)

                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSimSlowGammaSpecific():
    size = 500
    agent_n = 500
    degree = 4
    sigmas = [1, 2, 3, 4]

    repeat = 500
    steps = 3000

    r = 0.2
    #mus = np.linspace(0, 0.25, 41)
    #ms = mus * r
    mus = [0.02]

    cs = np.linspace(0.0, 0.4, 201)

    counter = 0
    for sigma in sigmas:
        for mu in mus:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int agent_n=%d\n" % agent_n)
                    f.write("double sigma=%f\n" % sigma)

                    f.write("double r=%f\n" % r)
                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("int repeat=%d\n" % repeat)
                    f.write("int steps=%d\n" % steps)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4SlowGamma():
    size = 500
    degree = 4
    sigmas = [1, 2, 3, 4]

    h = 1.
    repeat = 3000
    times = 2000.

    mus = np.linspace(0, 0.25, 126)
    #ks = np.append(np.linspace(0.0, 0.2, 101), np.linspace(0.2, 1, 201)[1:])
    cs = np.linspace(0.0, 0.3, 61)

    counter = 0
    for sigma in sigmas:
        for mu in mus:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("double sigma=%f\n" % sigma)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double h=%f\n" % h)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4SlowGammaPecific():
    size = 1000
    degree = 4
    sigmas = [1, 2, 3, 4]

    h = 1.
    repeat = 3000
    times = 2000.

    #mus = np.linspace(0, 0.25, 126)
    mus = [0.05]
    #ks = np.append(np.linspace(0.0, 0.2, 101), np.linspace(0.2, 1, 201)[1:])
    cs = np.linspace(0.0, 0.4, 401)

    counter = 0
    for sigma in sigmas:
        for mu in mus:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("double sigma=%f\n" % sigma)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double h=%f\n" % h)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4FastGammaCSigma():
    size = 500
    degree = 4
    sigmas = np.arange(1, 5.5, 0.5)

    h = 0.08
    repeat = 2000
    times = 20000.

    mus = [10]
    cs = np.linspace(0.001, 0.006, 101)

    counter = 0
    for sigma in sigmas:
        for mu in mus:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("double sigma=%f\n" % sigma)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double h=%f\n" % h)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4FastGammaCSize():
    degree = 4
    sigmas = [3]
    sizes = [150, 200, 250, 300, 350, 400, 450]

    h = 0.08
    repeat = 2000
    times = 20000.

    mus = [10]
    cs = np.linspace(0.002, 0.012, 101)

    counter = 0
    for size in sizes:
        for sigma in sigmas:
            for mu in mus:
                for c in cs:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int size=%d\n" % size)
                        f.write("int degree=%d\n" % degree)
                        f.write("double sigma=%f\n" % sigma)

                        f.write("double mu=%f\n" % mu)
                        f.write("double c=%f\n" % c)
                        f.write("double h=%f\n" % h)

                        f.write("int repeat=%d\n" % repeat)
                        f.write("double times=%f\n" % times)
                        counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4SlowNormalC():
    size = 800
    degree = 4
    sigmas = [0.01, 0.03, 0.05]

    h = 1.
    repeat = 2000
    times = 2000.

    mus = np.linspace(0, 0.25, 51)
    #ks = np.append(np.linspace(0.0, 0.2, 101), np.linspace(0.2, 1, 201)[1:])
    cs = np.linspace(0.0, 1, 101)

    counter = 0
    for sigma in sigmas:
        for mu in mus:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("double sigma=%f\n" % sigma)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double h=%f\n" % h)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4SlowNormalCSpecific():
    size = 1000
    degree = 4
    sigmas = [0, 0.01, 0.05, 0.1, 0.15]

    h = 1.
    repeat = 4000
    times = 2000.

    mus = [0.05]
    #ks = np.append(np.linspace(0.0, 0.2, 101), np.linspace(0.2, 1, 201)[1:])
    cs = np.linspace(0.0, 1, 1001)

    counter = 0
    for sigma in sigmas:
        for mu in mus:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("double sigma=%f\n" % sigma)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double h=%f\n" % h)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4FastNormalC():
    degree = 4
    sigmas = [0.01, 0.03, 0.05]

    h = 0.08
    repeat = 2000
    times = 20000.

    mus = [10]
    #ks = np.append(np.linspace(0.0, 0.2, 101), np.linspace(0.2, 1, 201)[1:])
    #cs = np.linspace(0.0, 0.01, 101)
    cs = np.linspace(0.0005, 0.005, 126)
    size_cs = [
        (200, np.linspace(0.0, 0.015, 151)),
        (400, np.linspace(0.0, 0.0075, 76)),
        (600, np.linspace(0.0, 0.005, 101)),
        (800, np.linspace(0.0, 0.0038, 77)),
        (1000, np.linspace(0.0, 0.003, 61))
    ]

    counter = 0
    for sigma in sigmas:
        for mu in mus:
            for size, cs in size_cs:
                for c in cs:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int size=%d\n" % size)
                        f.write("int degree=%d\n" % degree)
                        f.write("double sigma=%f\n" % sigma)

                        f.write("double mu=%f\n" % mu)
                        f.write("double c=%f\n" % c)
                        f.write("double h=%f\n" % h)

                        f.write("int repeat=%d\n" % repeat)
                        f.write("double times=%f\n" % times)
                        counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4SlowBiC():
    size = 500
    degree = 6

    h = 1.
    repeat = 4000
    times = 2000.

    mus = [0.05]

    c_hs = np.linspace(0.1, 1, 181)
    c_ls = [0.1]
    c_ratios = np.linspace(0, 1, 101)

    counter = 0
    for mu in mus:
        for c_l in c_ls:
            for c_h in c_hs:
                for c_ratio in c_ratios:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int size=%d\n" % size)
                        f.write("int degree=%d\n" % degree)
                        f.write("double mu=%f\n" % mu)

                        f.write("double chigh=%f\n" % c_h)
                        f.write("double clow=%f\n" % c_l)
                        f.write("double cratio=%f\n" % c_ratio)

                        f.write("double h=%f\n" % h)
                        f.write("int repeat=%d\n" % repeat)
                        f.write("double times=%f\n" % times)
                        counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4SlowGridBiC():
    grid_length = 40

    h = 1.
    repeat = 2000
    times = 2000.

    mus = [0.05]

    c_hs = np.linspace(0.1, 1, 91)
    c_ls = [0.1]
    c_ratios = np.linspace(0, 1, 51)

    counter = 0
    for mu in mus:
        for c_l in c_ls:
            for c_h in c_hs:
                for c_ratio in c_ratios:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int l=%d\n" % grid_length)
                        f.write("double mu=%f\n" % mu)

                        f.write("double chigh=%f\n" % c_h)
                        f.write("double clow=%f\n" % c_l)
                        f.write("double cratio=%f\n" % c_ratio)

                        f.write("double h=%f\n" % h)
                        f.write("int repeat=%d\n" % repeat)
                        f.write("double times=%f\n" % times)
                        counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genRK4FastBiC():
    size = 200
    degree = 6

    h = 0.08
    repeat = 4000
    times = 20000.

    mus = [10]

    c_ls = [1/float(size*2)]
    # for all
    #c_hs = np.linspace(0, 0.03, 151)
    #c_ratios = np.linspace(0.2, 1, 81)
    # for specific
    c_hs = np.linspace(0, 0.03, 601)
    c_ratios = [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]

    counter = 0
    for mu in mus:
        for c_l in c_ls:
            for c_h in c_hs:
                for c_ratio in c_ratios:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int size=%d\n" % size)
                        f.write("int degree=%d\n" % degree)
                        f.write("double mu=%f\n" % mu)

                        f.write("double chigh=%f\n" % c_h)
                        f.write("double clow=%f\n" % c_l)
                        f.write("double cratio=%f\n" % c_ratio)

                        f.write("double h=%f\n" % h)
                        f.write("int repeat=%d\n" % repeat)
                        f.write("double times=%f\n" % times)
                        counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSlowLine():
    l = 100

    h = 0.8
    repeat = 1000
    times = 2000.

    #mus = np.linspace(0, 0.25, 101)
    mus = np.linspace(0.25, 1, 301)

    #cs = np.linspace(0., 1, 201)
    cs = np.linspace(0., 0.5, 101)

    counter = 0
    for mu in mus:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int l=%d\n" % l)
                f.write("double mu=%f\n" % mu)

                f.write("double c=%f\n" % c)

                f.write("double h=%f\n" % h)
                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSlowLineSpeed():
    l = 2000

    t = 0.01
    h = 0.4
    repeat = 1000
    times = 4000.

    mus = np.linspace(0, 1, 40)

    cs = np.linspace(0., 0.5, 20)

    counter = 0
    for mu in mus:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int l=%d\n" % l)
                f.write("double mu=%f\n" % mu)
                f.write("double t=%f\n" % t)

                f.write("double c=%f\n" % c)

                f.write("double h=%f\n" % h)
                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genWaveFront():
    l = 2000

    t = 0.01
    h = 0.4
    repeat = 1000
    times = 4000.

    mus = np.linspace(0., 0.5, 100)

    cs = np.linspace(0., 1, 200)

    counter = 0
    for mu in mus:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int l=%d\n" % l)
                f.write("double mu=%f\n" % mu)
                f.write("double t=%f\n" % t)

                f.write("double c=%f\n" % c)

                f.write("double h=%f\n" % h)
                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genDiffusion():
    sizes = [1000, 900, 800, 700, 600, 500, 400, 300, 200]
    degrees = [4, 6, 8, 10]

    margin = 0.000001

    repeat = 1000
    times = 4000.

    mu_h = [
        (0.1, 1),
        (1, 0.8),
        (10, 0.08),
        (100, 0.008)
    ]

    counter = 0
    for size in sizes:
        for degree in degrees:
            for mu, h in mu_h:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("double mu=%f\n" % mu)

                    f.write("double h=%f\n" % h)
                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    f.write("double margin=%f\n" % margin)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


# multiple seeds
def genSeedsRegular():
    size = 300
    degree = 6
    h = 1
    repeat = 1000
    times = 20000.
    # gen all configs
    mus = np.linspace(0, 0.25, 51)
    cs = np.linspace(0., 1, 201)

    counter = 0
    for mu in mus:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int size=%d\n" % size)
                f.write("int degree=%d\n" % degree)

                f.write("double mu=%f\n" % mu)
                f.write("double c=%f\n" % c)
                f.write("double h=%f\n" % h)

                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

# gen meta
    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSeedsTri():
    h = 1.
    l = 20
    # for all
    #repeat = 3000

    # for speed
    repeat = 1000
    times = 2000.

    # for sweep
    #mus = np.linspace(0, 0.25, 126)
    #cs = np.linspace(0, 0.3, 101)

    #for speed
    mus = np.linspace(0, 0.25, 51)
    cs = np.linspace(0., 1, 201)

    # for specific
    #mus = [0.05, 0.138, 0.2]
    #cs = np.linspace(0.0, 0.4, 101)

    counter = 0
    for mu in mus:
        for c in cs:
            filename = "./config/%d.cfg" % counter
            with open(filename, "w") as f:
                f.write("int l=%d\n" % l)
                f.write("double mu=%f\n" % mu)
                f.write("double c=%f\n" % c)
                f.write("double h=%f\n" % h)

                f.write("int repeat=%d\n" % repeat)
                f.write("double times=%f\n" % times)
                counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSeedsEr():
    size = 1000
    degree = 8

    h = 1.
    repeat = 1000
    times = 2000.

    seeds = [1, 2, 3, 5, 10, 20, 50, 100]
    mus = [0.01]
    cs = np.linspace(0, 0.25, 251)

    counter = 0
    for seed in seeds:
        for mu in mus:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int seed=%d\n" % seed)
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double h=%f\n" % h)

                    f.write("int repeat=%d\n" % repeat)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genSeedLine():
    l = 1000

    h = 1
    times = 4000.

    mus = np.linspace(0., 0.3, 101)

    seeds = [1, 2, 10, 50]

    cs = np.linspace(0., 1, 201)

    counter = 0
    for seed in seeds:
        for mu in mus:
            for c in cs:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int l=%d\n" % l)
                    f.write("int seed=%d\n" % seed)
                    f.write("double mu=%f\n" % mu)

                    f.write("double c=%f\n" % c)

                    f.write("double h=%f\n" % h)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genLocal():
    size = 1000
    degree = 4
    repeat = 1000

    h = 1.
    times = 4000.

    #mus = np.linspace(0., 0.3, 101)
    mus = [0.05]
    #cs = np.linspace(0., 1, 101)
    cs = [0.24, 0.34, 0.4, 0.5]
    #ps = [0.005, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5]
    #ps = [0.995, 0.99, 0.95, 0.9, 0.8, 0.7, 0.6]
    ps = np.linspace(0., 1., 201)

    counter = 0
    for mu in mus:
        for c in cs:
            for p in ps:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int size=%d\n" % size)
                    f.write("int degree=%d\n" % degree)
                    f.write("int repeat=%d\n" % repeat)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double p=%f\n" % p)

                    f.write("double h=%f\n" % h)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genThreshold2dGrid():
    length = 40
    repeat = 1000

    h = 1.
    times = 4000.

    mus = [0.05]
    cs = np.linspace(0., 1, 101)
    ps = [0.005, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.95, 0.99, 0.995]

    counter = 0
    for mu in mus:
        for c in cs:
            for p in ps:
                filename = "./config/%d.cfg" % counter
                with open(filename, "w") as f:
                    f.write("int l=%d\n" % length)
                    f.write("int repeat=%d\n" % repeat)

                    f.write("double mu=%f\n" % mu)
                    f.write("double c=%f\n" % c)
                    f.write("double p=%f\n" % p)

                    f.write("double h=%f\n" % h)
                    f.write("double times=%f\n" % times)
                    counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genThreshold2dGridSection():
    lengths = [20, 30, 40, 50]
    repeat = 1000

    h = 1.
    times = 4000.

    mus = [0.05]
    cs = [0.25]
    ps = np.linspace(0, 1., 201)

    counter = 0
    for length in lengths:
        for mu in mus:
            for c in cs:
                for p in ps:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int l=%d\n" % length)
                        f.write("int repeat=%d\n" % repeat)

                        f.write("double mu=%f\n" % mu)
                        f.write("double c=%f\n" % c)
                        f.write("double p=%f\n" % p)

                        f.write("double h=%f\n" % h)
                        f.write("double times=%f\n" % times)
                        counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genLocalER():
    sizes = [1000]
    degree = 6
    repeat = 1000

    h = 1.
    times = 4000.

    #mus = np.linspace(0., 0.3, 101)
    mus = [0.05]
    #cs = np.linspace(0., 1, 101)
    cs = [0.19, 0.24, 0.3]
    #ps = [0.005, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5]
    #ps = [0.995, 0.99, 0.95, 0.9, 0.8, 0.7, 0.6]
    #ps = np.linspace(0., 1., 21)
    ps = np.linspace(0., 1., 101)

    counter = 0
    for size in sizes:
        for mu in mus:
            for c in cs:
                for p in ps:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int size=%d\n" % size)
                        f.write("int degree=%d\n" % degree)
                        f.write("int repeat=%d\n" % repeat)

                        f.write("double mu=%f\n" % mu)
                        f.write("double c=%f\n" % c)
                        f.write("double p=%f\n" % p)

                        f.write("double h=%f\n" % h)
                        f.write("double times=%f\n" % times)
                        counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)


def genBootStrap():
    sizes = [1000, 2000, 5000, 10000, 20000, 50000]
    degree = 4
    repeat = 1000

    h = 1.
    times = 4000.

    #mus = np.linspace(0., 0.3, 101)
    mus = [0.05]
    #cs = np.linspace(0., 1, 101)
    #cs = [0.24, 0.34, 0.4, 0.5]
    cs = [0.24]
    #ps = [0.005, 0.01, 0.05, 0.1, 0.2, 0.3, 0.4, 0.5]
    #ps = [0.995, 0.99, 0.95, 0.9, 0.8, 0.7, 0.6]
    ps = np.linspace(0., 0.2, 201)

    counter = 0
    for size in sizes:
        for mu in mus:
            for c in cs:
                for p in ps:
                    filename = "./config/%d.cfg" % counter
                    with open(filename, "w") as f:
                        f.write("int size=%d\n" % size)
                        f.write("int degree=%d\n" % degree)
                        f.write("int repeat=%d\n" % repeat)

                        f.write("double mu=%f\n" % mu)
                        f.write("double c=%f\n" % c)
                        f.write("double p=%f\n" % p)

                        f.write("double h=%f\n" % h)
                        f.write("double times=%f\n" % times)
                        counter += 1

    with open("./config/meta.cfg", "w") as f:
        for i in range(counter):
            f.write("%d\n" % i)

if __name__ == "__main__":
    genThreshold2dGridSection()
