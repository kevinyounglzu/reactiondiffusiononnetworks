# -*- coding: utf8 -*-
import numpy as np
import os


def getNum(the_string):
    return float(the_string[4:])

if __name__ == "__main__":
    base = "./data/er/"
    files = os.listdir(base)

    with open("./data/ermeta.txt", "w") as meta:
        for ff in files:
            with open(base + ff) as f:
                rstring = f.readline()
                r = getNum(rstring)
                mstring = f.readline()
                m = getNum(mstring)
                degreeString = f.readline()
                degree = getNum(degreeString)

            datas = np.loadtxt(base + ff)
            for data in datas:
                if np.average(data[400:]) < 1.1:
                    break
            k = data[0]
            meta.write("%f %f %f %f\n" % (r, m, k, degree))
